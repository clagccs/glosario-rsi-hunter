# RSI Hunter Glossary

The following list contains the description and explanation related to all the terms, abbreviations and concepts you can find on the website [rsihunter](https://rsihunter.com/)
<br>
<br>

## RSI

Also known as Relative Strength Index is a momentum oscillator indicator, that measures the magnitude of recent price changes to evaluate overbought or oversold conditions in the price of a stock The RSI takes values between 0 (maximum oversold) and 100 (maximum overbought), in **rsihunter** the oversold and overbought zones are adjusted to 30 and 70

`fuente: https://www.fidelity.com/learning-center/trading-investing/technical-analysis/technical-indicator-guide/RSI`

![enter image description here](https://gitlab.com/clagccs/glosario-rsi-hunter/raw/master/imagenes/rsi.png)

To compute the RSI you can use the following formula
$$RSI = 100 – [100 / ( 1 + (Average of Upward Price Change / Average of Downward Price Change )  )  ]$$

`Está bien agregar información de posible momento de compra/venta?`

> The RSI indicator is represented in rsihunter as the progress bar located above each chart, if the bar is shown in red it means that the asset is being **sold** and **can be a signal for a possible sale**, if the bar has a green color it means that the asset is in a state of **overbuy** so **can be a signal for a possible buy. [^enlacersi]**.

<br>
<br>

## AVG Volume

AVG Volume is the average number of shares traded in a given period of time. **RSI Hunter** calculates the AVG Volume acording to the last **10 periods**, you can change these periods to days, hours or minutes in the timeframe selector located at the top of the page.

![enter image description here](https://gitlab.com/clagccs/glosario-rsi-hunter/raw/master/imagenes/timeframe.png)

> Trading activity relates to the liquidity of a security — when average daily trading volume is high, the stock can be easily traded and has high liquidity; if trading volume isn't very high, the security will tend to be less expensive because people are not as willing to buy it. As a result, average daily trading volume can have an effect on the price of the security [^enlaceavg].

<br>
<br>

## VWAP

The volume weighted average price (VWAP) is equals the dollar value of all trading periods divided by the total trading volume for the current day.

$$VWAP = \sum_{}^{} \dfrac{NumberShares Bought * Share Price}{Total Shares Bought}$$

> The VWAP identifies the true average price of a stock by factoring the volume of transactions at a specific price point and not based on the closing price. The theory is that if the price of a buy trade is lower than the VWAP, it is a good trade. The opposite is also true

<br>
<br>

## ATH

All time high. indicates the maximum historical value achieved by a cryptomoneda >Technically it is a very bullish signal when a stock crosses its previous all time high price and makes a new high.

<br>
<br>

## Moving Average (MA)

The moving average indicator is a simple technical analysis tool that smooths out price data by computing the average price of a security over a specific number of periods It is calculated by adding recent closing prices and then dividing that by the number of time periods in the calculation average. >in RSI hunter you can see the moving average of the last 28,56,100 and 200 periods, including the 1st and 2nd standard deviation and the periods that have been below or above the current price.

<br>
<br>

## Pivot Point

A pivot point is a price level that is used by traders as a possible indicator of market movement. A pivot point is calculated as an average of significant prices (high, low, close) from the performance of a market in the prior trading period. If the market in the following period trades above the pivot point it is usually evaluated as a bullish sentiment, whereas trading below the pivot point is seen as bearish.[^enlacepp]

$$Pivot = (Previous High + Previous Low + Previous Close)/3$$

<br>
<br>

## S1,S2,S3

These are known as support points and represent the level at which buyers tend to purchase or enter into a stock, prices tend not to exceed this point, instead a bounce off would be observed, however if the price were to cross this point, it could mean that the price would continue to rise to the next support point.

$$S1 = Pivot − (Previous High − Pivot) = 2 × Pivot − Previous Hight$$
$$S2 = Pivot − (Previous High − Previous Low)$$
$$S3 =S1 − (Previous Hight − Previous Low)$$

<br>
<br>

## R1,R2,R3

These are known as resistance level, the price tends to find resistance as it rises after this point, that means it will tend to bounce off rather than break through it, however once the price crosses this value it is likely that it will continue to fall until it gets another resistance.

$$R1 = 2×Pivot − Previous Low$$
$$R2 = Pivot + (Previous Hight − Previous Low)$$
$$R3= R1 + (Previous Hight − Previous Low)$$

[^enlaceavg]:

https://www.investopedia.com/terms/a/averagedailytradingvolume.asp

[^enlacepp]:

https://en.wikipedia.org/wiki/Pivot_point_(technical_analysis)

> Written with [StackEdit](https://stackedit.io/).

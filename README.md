# Glosario de términos utilizados en RSI Hunter

Este repositorio contiene un listado de los términos financieros utilizados en el sitio web de [RSI Hunter](rsihunter.com) en formato html y markdown, así comot también una guía en caso que se requiera realizar modificaciones y/o actualizaciones

## Ruta de acceso

[glosario de términos](https://clagccs.gitlab.io/glosario-rsi-hunter/)

## Actualización y modificaciones

En caso de requerir modificar o actualizar el archivo html el procedimiento para realizarlo sería el siguiente

1. Descargue el repositorio y abra el documento glossary.md

2. Ingrese al sitio web de [stackedit](https://stackedit.io/) donde podrá editar y ver en tiempo real las modificaciones en el documento

3. Una vez finalizada la edición del documento guarde las modificaciones en el archivo glossary.md

4. En la esquina superior derecha encontrará el menu principal, click allí, luego en "export to disk" / "export as HTML" y marque la opción "Styled HTML with toc"

5. Una vez descargado el documento remplace el archivo index.html con el nuevo archivo

6. haga un "git commit..." y un "git push..." para actualizar los datos en el repositorio remoto, automaticamente los cambios se deberían ver reflejados en la web

7. Puede acceder al sitio por esta [ruta](https://clagccs.gitlab.io/glosario-rsi-hunter/)

<br>
<br>
<br>
<br>
<br>
<br>

## Imágenes del proceso de exportar a HTML

![enter image description here](https://gitlab.com/clagccs/glosario-rsi-hunter/raw/master/imagenes/menu.png)

![enter image description here](https://gitlab.com/clagccs/glosario-rsi-hunter/raw/master/imagenes/export.png)

![enter image description here](https://gitlab.com/clagccs/glosario-rsi-hunter/raw/master/imagenes/htmlexport.png)
